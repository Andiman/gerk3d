package de.gelbesp.www.gerk3d;


import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.CheckBox;
import android.widget.TextView;


public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener{

    private TextView tvAccDataX, tvAccDataY, tvAccDataZ;
    private CheckBox checkBoxAverageOver1Second;
    private Sensor sensAccData;
    private SensorManager smAccData;

    private int currentNrOfValues;
    private float xA, yA, zA;

    private long cts;
    private short sensEvThSec;
    private TextView tvSensEvThSec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        smAccData = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensAccData = smAccData.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        smAccData.registerListener(this, sensAccData, SensorManager.SENSOR_DELAY_FASTEST);

        tvAccDataX = (TextView)findViewById(R.id.tvAccDataX);
        tvAccDataY = (TextView)findViewById(R.id.tvAccDataY);
        tvAccDataZ = (TextView)findViewById(R.id.tvAccDataZ);
        checkBoxAverageOver1Second = (CheckBox)findViewById(R.id.checkBoxAverageOver1Second);

        tvSensEvThSec = (TextView)findViewById(R.id.tvSensEvThSec);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!checkBoxAverageOver1Second.isChecked()) {
            tvAccDataX.setText("X: " + event.values[0]);
            tvAccDataY.setText("Y: " + event.values[1]);
            tvAccDataZ.setText("Z: " + event.values[2]);
        } else {
            xA += event.values[0];
            yA += event.values[1];
            zA += event.values[2];
            currentNrOfValues++;

            if (currentNrOfValues >= 120) {
                tvAccDataX.setText("X: " + (xA / 120.0f));
                tvAccDataY.setText("Y: " + (yA / 120.0f));
                tvAccDataZ.setText("Z: " + (zA / 120.0f));

                xA = 0;
                yA = 0;
                zA = 0;
                currentNrOfValues = 0;
            }
        }

        if (cts != (System.nanoTime() / 1000000000)) {
            tvSensEvThSec.setText("Sensor Events /sec : " + sensEvThSec);
            sensEvThSec = 0;
            cts = System.nanoTime() / 1000000000;
        } else {
            sensEvThSec++;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }
}
