package de.gelbesp.www.gerk3d;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DrawMovementActivity extends AppCompatActivity implements SurfaceHolder.Callback, SensorEventListener, Runnable {

    private Thread drawThread = null;
    private SurfaceView surfaceView;
    private volatile SurfaceHolder surfaceHolder;
    private volatile boolean drawRunning = false;
    private Paint paint = new Paint();

    private float oldPosX = 0;
    private float oldPosY = 0;
    private float surfaceWidth = 0;
    private float surfaceHeight = 0;
    private float scaleMovement = 1;

    private Sensor sensAccData;
    private SensorManager smAccData;

    private boolean calDone = false;
    private int calQuantity = 1024;
    private int calIteration = 0;
    private float calNoiseIgnoreFactor = 1.05f;
    private float calIntegratorDecayFactor = 0.985f;
    private float calLinAccX = 0;
    private float calLinAccMinX, calLinAccMaxX, calLinAccStdVarX = 0;
    private float calLinAccY = 0;
    private float calLinAccMinY, calLinAccMaxY, calLinAccStdVarY = 0;
    private int calSmoothingWindow = 4;
    private float[] calSmoothingWindowDataArrayX = new float[calSmoothingWindow];
    private float[] calSmoothingWindowDataArrayY = new float[calSmoothingWindow];

    private float deltaTeventDataS = 0;
    private long lastTimeStampMS = 0;

    private float lastAccEventDataX = 0;
    private float lastAccEventDataY = 0;

    private float lastVelEventDataX = 0;
    private float lastVelEventDataY = 0;

    private float lastPosEventDataX = 0;
    private float lastPosEventDataY = 0;

    private BlockingQueue<Float> sensorAccEventDataX = new LinkedBlockingQueue<>();
    private BlockingQueue<Float> sensorAccEventDataY = new LinkedBlockingQueue<>();


    //DEBUG VARS
    private String sensorAccDataForPrintOut = new String();
    private int sensorAccDataForPrintOutCount = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_movement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        surfaceView = (SurfaceView)findViewById(R.id.svDrawMovemet);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);

        smAccData = (SensorManager)getSystemService(SENSOR_SERVICE);
        //sensAccData = smAccData.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensAccData = smAccData.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        smAccData.registerListener(this, sensAccData, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onDestroy() {
        //System.out.println("cAxS = [" + sensorAccDataForPrintOut + "]");

        super.onDestroy();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        //paint.setStyle(Paint.Style.STROKE);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(16);

        drawThread = new Thread(this);
        drawRunning = true;
        drawThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        surfaceWidth = (float)width;
        surfaceHeight = (float)height;
        oldPosX = (float)(width / 2);
        oldPosY = (float)(height / 2);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        drawRunning = false;
        while(retry){
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
                // Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void tryDrawing(SurfaceHolder holder) {
        //Log.i(TAG, "Trying to draw...");

        Canvas canvas = holder.lockCanvas();
        if (canvas == null) {
            //Log.e(TAG, "Cannot draw onto the canvas as it's null");
        } else {
            drawMyStuff(canvas);
            holder.unlockCanvasAndPost(canvas);
        }
    }

    private void drawMyStuff(final Canvas canvas) {

        float sensX;
        float sensY;

        paint.setColor(0xff0000ff);

        while (!sensorAccEventDataX.isEmpty()
                && !sensorAccEventDataY.isEmpty()) {
            sensX = -sensorAccEventDataX.remove();
            sensY = sensorAccEventDataY.remove();

            /*canvas.drawLine(oldPosX, oldPosY,
                    oldPosX + (sensX * scaleMovement), oldPosY + (sensY * scaleMovement), paint);*/

            canvas.drawPoint((surfaceWidth / 2) + sensX, (surfaceHeight / 2) + sensY, paint);

            oldPosX = oldPosX + (sensX * scaleMovement);
            oldPosY = oldPosY + (sensY * scaleMovement);

            if (oldPosX > surfaceWidth) {
                oldPosX = surfaceWidth;
            } else if (oldPosX < 0) {
                oldPosX = 0;
            }
            if (oldPosY > surfaceHeight) {
                oldPosY = surfaceHeight;
            } else if (oldPosY < 0) {
                oldPosY = 0;
            }
        }

        /*paint.setColor(0xff0000ff);
        canvas.drawLine((w/2), (h/2),
                (w/2) + (sensX * 100), (h/2) + (sensY * 100), paint);*/
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!calDone) {
            if (calIteration == 0) {
                calLinAccMinX = event.values[0];
                calLinAccMaxX = event.values[0];

                calLinAccMinY = event.values[1];
                calLinAccMaxY = event.values[1];

                for (int i = 0; i < calSmoothingWindow; i++) {
                    calSmoothingWindowDataArrayX[i] = 0;
                    calSmoothingWindowDataArrayY[i] = 0;
                }
            }

            calLinAccX += event.values[0];
            if (event.values[0] < calLinAccMinX) {
                calLinAccMinX = event.values[0];
            } else if (event.values[0] > calLinAccMaxX) {
                calLinAccMaxX = event.values[0];
            }

            calLinAccY += event.values[1];
            if (event.values[1] < calLinAccMinY) {
                calLinAccMinY = event.values[1];
            } else if (event.values[1] > calLinAccMaxY) {
                calLinAccMaxY = event.values[1];
            }

            calIteration++;

            if (calIteration >= calQuantity) {
                calLinAccX /= calQuantity;
                calLinAccY /= calQuantity;

                //calLinAccStdVarX = calLinAccMaxX - calLinAccMinX;
                //calLinAccStdVarY = calLinAccMaxY - calLinAccMinY;

                calDone = true;

                Toast toast = Toast.makeText(getApplicationContext(),
                        "Calibration successfully Done! (Hold phone very still!) cX: "
                                + calLinAccX + " cY: " + calLinAccY,
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        } else {

            long timeStampNowMS = System.nanoTime();
            deltaTeventDataS = (float)(((double)timeStampNowMS - (double)lastTimeStampMS) / (double)1000000000.0f);
            lastTimeStampMS = timeStampNowMS;

            if (deltaTeventDataS > (1f/10f)) {
                deltaTeventDataS = 1f/10f;
            } else if (deltaTeventDataS < (1f/1000f)) {
                deltaTeventDataS = 1f/1000f;
            }

            //System.out.println("dTS = " + deltaTeventDataS);

            //System.out.println("cAx = " + (event.values[0] - calLinAccX));

            float currentAccX = 0;
            float currentAccY = 0;

            if ((event.values[0] < calLinAccMinX * calNoiseIgnoreFactor)
                    || (event.values[0] > calLinAccMaxX * calNoiseIgnoreFactor)) {
                currentAccX = event.values[0] - calLinAccX;

                //System.out.println("cAx = " + currentAccX);
                //sensorAccDataForPrintOut += Float.toString(currentAccX) + " ";
            }

            if ((event.values[1] < calLinAccMinY * calNoiseIgnoreFactor)
                    || (event.values[1] > calLinAccMaxY * calNoiseIgnoreFactor)) {
                currentAccY = event.values[1] - calLinAccY;
            }


            //float currentAccX = event.values[0] - calLinAccX;
            //float currentAccY = event.values[1] - calLinAccY;

            //float currentAccX = 0;
            //float currentAccY = 0;
            for (int i = 1; i < calSmoothingWindow; i++) {
                calSmoothingWindowDataArrayX[i-1] = calSmoothingWindowDataArrayX[i];
                calSmoothingWindowDataArrayY[i-1] = calSmoothingWindowDataArrayY[i];

                currentAccX += calSmoothingWindowDataArrayX[i-1];
                currentAccY += calSmoothingWindowDataArrayY[i-1];

                if (i == calSmoothingWindow -1) {
                    calSmoothingWindowDataArrayX[i] = event.values[0] - calLinAccX;
                    currentAccX += calSmoothingWindowDataArrayX[i];
                    calSmoothingWindowDataArrayY[i] = event.values[1] - calLinAccY;
                    currentAccY += calSmoothingWindowDataArrayY[i];
                }
            }
            currentAccX /= (float)calSmoothingWindow;
            currentAccY /= (float)calSmoothingWindow;



            /*sensorAccDataForPrintOut += Float.toString(currentAccX) + " ";
            sensorAccDataForPrintOutCount++;
            if (sensorAccDataForPrintOutCount >= 200) {
                sensorAccDataForPrintOutCount = 0;
                System.out.println("cAx = " + sensorAccDataForPrintOut);
                sensorAccDataForPrintOut = "";
            }*/


            float currentVelX = lastVelEventDataX + (((lastAccEventDataX + currentAccX) / 2) * deltaTeventDataS);
            float currentVelY = lastVelEventDataY + (((lastAccEventDataY + currentAccY) / 2) * deltaTeventDataS);

            float currentPosX = lastPosEventDataX + (((lastVelEventDataX + currentVelX) / 2) * deltaTeventDataS);
            float currentPosY = lastPosEventDataY + (((lastVelEventDataY + currentVelY) / 2) * deltaTeventDataS);


            sensorAccEventDataX.add(currentPosX * 1000);
            sensorAccEventDataY.add(currentPosY * 1000);

            //sensorAccEventDataX.add(currentVelX);
            //sensorAccEventDataY.add(currentVelY);


            lastAccEventDataX = currentAccX;
            lastAccEventDataY = currentAccY;

            lastVelEventDataX = currentVelX * calIntegratorDecayFactor; // Integrators have to fade to zero otherwise unstable
            lastVelEventDataY = currentVelY * calIntegratorDecayFactor;

            lastPosEventDataX = currentPosX;// * calIntegratorDecayFactor;
            lastPosEventDataY = currentPosY;// * calIntegratorDecayFactor;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void run() {
        Canvas canvas = null;

        while(drawRunning){
            /*if(surfaceHolder.getSurface().isValid()){
                Canvas canvas = surfaceHolder.lockCanvas();
                //... actual drawing on canvas

                drawMyStuff(canvas);

                surfaceHolder.unlockCanvasAndPost(canvas);
            }*/

            try {
                canvas = surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    drawMyStuff(canvas);
                }
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}