package de.gelbesp.www.gerk3d;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

//import org.wiigee.control.AndroidWiigee; // using own slightly modified version
import org.wiigee.event.GestureEvent;
import org.wiigee.event.GestureListener;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;


public class GestureRecognitionActivity extends AppCompatActivity {

    private static final int _TRAIN_BUTTON = 0x01;
    private static final int _SAVE_BUTTON = 0x02;
    private static final int _RECOGNIZE_BUTTON = 0x03;

    private AndroidWiigee wiigee;
    private Logger logger;

    private TextToSpeech textToSpeech;

    private boolean isRecording;
    private boolean isRecognizing;

    private int currentNumberOfTrainedGestures = 0;
    private int currentNumberOfTrainingGesturesForCurrentGesture = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture_recognition);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wiigee = new AndroidWiigee(this);
        logger = new Logger((TextView) findViewById(R.id.tv_logText), 12);
        isRecording = false;
        isRecognizing = false;

        //wiigee.getDevice().saveGesture(0, "peter");
        //wiigee.getDevice().getProcessingUnit().saveGesture(0, "Circle");

        wiigee.setTrainButton(_TRAIN_BUTTON);
        wiigee.setCloseGestureButton(_SAVE_BUTTON);
        wiigee.setRecognitionButton(_RECOGNIZE_BUTTON);
        wiigee.addGestureListener(new GestureListener() {

            @Override
            public void gestureReceived(GestureEvent event) {
                logger.addLog("RECO: gest " + event.getId() + " with prob " + event.getProbability());

                float prob = Math.round(event.getProbability() * 1000.0f) / 1000.0f;
                CharSequence toSpeak = "Recognized gesture: " + event.getId() + " with probability: " + prob;
                textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        });

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                /*if(status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.ENGLISH);
                }*/
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeech.setLanguage(Locale.US);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Text to speech feature missing on you phone.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        Button buttonStartTraining = (Button)findViewById(R.id.button_start_training);

        buttonStartTraining.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Button btn = (Button) v;
                        if (isRecording) {
                            btn.setText(R.string.button_start_training);
                            isRecording = false;
                            wiigee.getDevice().fireButtonReleasedEvent(_TRAIN_BUTTON);

                            currentNumberOfTrainingGesturesForCurrentGesture++;

                            logger.addLog("TRAIN:stopped rec of training gesture");

                            /*CharSequence toSpeak = "stopped recording of training gesture";
                            textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_ADD, null, null);*/
                        } else {
                            btn.setText(R.string.button_stop_training);
                            isRecording = true;
                            wiigee.getDevice().fireButtonPressedEvent(_TRAIN_BUTTON);

                            logger.addLog("TRAIN:started rec of train gesture "
                                    + currentNumberOfTrainingGesturesForCurrentGesture
                                    + " for gesture " + currentNumberOfTrainedGestures);

                            CharSequence toSpeak = "recording of training gesture "
                                    + currentNumberOfTrainingGesturesForCurrentGesture
                                    + " for gesture " + currentNumberOfTrainedGestures;
                            textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_ADD, null, null);
                        }
                    }
                }
        );

        Button buttonSaveGesture = (Button)findViewById(R.id.button_save_gesture);

        buttonSaveGesture.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        wiigee.getDevice().fireButtonPressedEvent(_SAVE_BUTTON);
                        wiigee.getDevice().fireButtonReleasedEvent(_SAVE_BUTTON);
                        logger.addLog("SAVE: saved "
                                + currentNumberOfTrainingGesturesForCurrentGesture
                                + " trainings for gesture "
                                + currentNumberOfTrainedGestures);

                        CharSequence toSpeak = "saved "
                                + currentNumberOfTrainingGesturesForCurrentGesture
                                + " trainings for gesture "
                                + currentNumberOfTrainedGestures;
                        textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);

                        currentNumberOfTrainedGestures++;
                        currentNumberOfTrainingGesturesForCurrentGesture = 0;
                    }
                }
        );

        Button buttonStartRecognizing = (Button)findViewById(R.id.button_start_recognizing);

        buttonStartRecognizing.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Button btn = (Button) v;
                        if(isRecognizing) {
                            btn.setText(R.string.button_start_recognizing);
                            isRecognizing = false;
                            wiigee.getDevice().fireButtonReleasedEvent(_RECOGNIZE_BUTTON);

                            logger.addLog("RECO:stopped recognizing");
                        } else {
                            btn.setText(R.string.button_stop_recognizing);
                            isRecognizing = true;
                            wiigee.getDevice().fireButtonPressedEvent(_RECOGNIZE_BUTTON);

                            logger.addLog("RECO:started recognizing");

                            CharSequence toSpeak = "recognizing";
                            textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_ADD, null, null);
                        }
                    }
                }
        );
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            wiigee.getDevice().setAccelerationEnabled(false);
        }
        catch(IOException e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            wiigee.getDevice().setAccelerationEnabled(true);
        }
        catch(IOException e) {
        }
    }

}
