package de.gelbesp.www.gerk3d;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;


public class CompassActivity extends AppCompatActivity implements SensorEventListener {

    private TextView tvCompDataX, tvCompDataY, tvCompDataZ;
    private Sensor sensCompData;
    private SensorManager smCompData;

    private long cts;
    private short sensEvThSec;
    private TextView tvSensEvThSec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        smCompData = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensCompData = smCompData.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);

        smCompData.registerListener(this, sensCompData, SensorManager.SENSOR_DELAY_FASTEST);

        tvCompDataX = (TextView)findViewById(R.id.tvCompDataX);
        tvCompDataY = (TextView)findViewById(R.id.tvCompDataY);
        tvCompDataZ = (TextView)findViewById(R.id.tvCompDataZ);

        tvSensEvThSec = (TextView)findViewById(R.id.tvSensEvThSec);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        tvCompDataX.setText("X: " + event.values[0]);
        tvCompDataY.setText("Y: " + event.values[1]);
        tvCompDataZ.setText("Z: " + event.values[2]);

        if (cts != (System.nanoTime() / 1000000000)) {
            tvSensEvThSec.setText("Sensor Events /sec : " + sensEvThSec);
            sensEvThSec = 0;
            cts = System.nanoTime() / 1000000000;
        } else {
            sensEvThSec++;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Not in use
    }
}
