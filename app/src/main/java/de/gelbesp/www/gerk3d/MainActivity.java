package de.gelbesp.www.gerk3d;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Button;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeActivity();
    }

    protected void initializeActivity() {
        Button buttonAccData = (Button)findViewById(R.id.buttonAccData);
        Button buttonCompData = (Button)findViewById(R.id.buttonCompData);
        Button buttonGyrData = (Button)findViewById(R.id.buttonGyrData);
        Button buttonPrLiData = (Button)findViewById(R.id.buttonPrLiData);
        Button buttonDrAccDa2D = (Button)findViewById(R.id.buttonDrAccDa2D);
        Button buttonGestRec = (Button)findViewById(R.id.buttonGestRec);

        buttonAccData.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intentViewAccData = new Intent(v.getContext(), AccelerometerActivity.class);
                        startActivity(intentViewAccData);
                    }
                }
        );

        buttonCompData.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intentViewCompData = new Intent(v.getContext(), CompassActivity.class);
                        startActivity(intentViewCompData);
                    }
                }
        );

        buttonGyrData.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intentViewCompData = new Intent(v.getContext(), GyroscopeActivity.class);
                        startActivity(intentViewCompData);
                    }
                }
        );

        buttonPrLiData.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intentViewCompData = new Intent(v.getContext(), ProximityLightActivity.class);
                        startActivity(intentViewCompData);
                    }
                }
        );

        buttonDrAccDa2D.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intentViewCompData = new Intent(v.getContext(), DrawMovementActivity.class);
                        startActivity(intentViewCompData);
                    }
                }
        );

        buttonGestRec.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intentViewCompData = new Intent(v.getContext(), GestureRecognitionActivity.class);
                        startActivity(intentViewCompData);
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}