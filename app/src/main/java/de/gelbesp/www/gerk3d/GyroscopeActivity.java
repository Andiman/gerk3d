package de.gelbesp.www.gerk3d;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.TextView;

public class GyroscopeActivity extends AppCompatActivity implements SensorEventListener {

    private TextView tvGyrDataX, tvGyrDataY, tvGyrDataZ;
    private Sensor sensGyrData;
    private SensorManager smGyrData;

    private long cts;
    private short sensEvThSec;
    private TextView tvSensEvThSec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gyroscope);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        smGyrData = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensGyrData = smGyrData.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        smGyrData.registerListener(this, sensGyrData, SensorManager.SENSOR_DELAY_FASTEST);

        tvGyrDataX = (TextView)findViewById(R.id.tvGyrDataX);
        tvGyrDataY = (TextView)findViewById(R.id.tvGyrDataY);
        tvGyrDataZ = (TextView)findViewById(R.id.tvGyrDataZ);

        tvSensEvThSec = (TextView)findViewById(R.id.tvSensEvThSec);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        tvGyrDataX.setText("X: " + event.values[0]);
        tvGyrDataY.setText("Y: " + event.values[1]);
        tvGyrDataZ.setText("Z: " + event.values[2]);

        if (cts != (System.nanoTime() / 1000000000)) {
            tvSensEvThSec.setText("Sensor Events /sec : " + sensEvThSec);
            sensEvThSec = 0;
            cts = System.nanoTime() / 1000000000;
        } else {
            sensEvThSec++;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Not in use
    }
}
