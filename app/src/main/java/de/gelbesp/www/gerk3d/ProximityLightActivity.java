package de.gelbesp.www.gerk3d;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.TextView;

public class ProximityLightActivity extends AppCompatActivity implements SensorEventListener {

    private TextView tvProximityData;
    private TextView tvLightData;
    private Sensor sensProximityData;
    private Sensor sensLightData;
    private SensorManager smPrLiData;

    private long ctsProx, ctsLight;
    private short sensEvThSecProx, sensEvThSecLight;
    private TextView tvSensEvThSecProx, tvSensEvThSecLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proximity_light);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        smPrLiData = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensProximityData = smPrLiData.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensLightData = smPrLiData.getDefaultSensor(Sensor.TYPE_LIGHT);

        smPrLiData.registerListener(this, sensProximityData, SensorManager.SENSOR_DELAY_FASTEST);
        smPrLiData.registerListener(this, sensLightData, SensorManager.SENSOR_DELAY_FASTEST);

        tvProximityData = (TextView)findViewById(R.id.tvProximityData);
        tvLightData = (TextView)findViewById(R.id.tvLightData);

        tvSensEvThSecProx = (TextView)findViewById(R.id.tvSensEvThSecProx);
        tvSensEvThSecLight = (TextView)findViewById(R.id.tvSensEvThSecLight);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            tvProximityData.setText("Proximity: " + event.values[0]);

            if (ctsProx != (System.nanoTime() / 1000000000)) {
                tvSensEvThSecProx.setText("Sensor Events /sec : " + sensEvThSecProx);
                sensEvThSecProx = 0;
                ctsProx = System.nanoTime() / 1000000000;
            } else {
                sensEvThSecProx++;
            }
        }

        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            tvLightData.setText("Light: " + event.values[0]);

            if (ctsLight != (System.nanoTime() / 1000000000)) {
                tvSensEvThSecLight.setText("Sensor Events /sec : " + sensEvThSecLight);
                sensEvThSecLight = 0;
                ctsLight = System.nanoTime() / 1000000000;
            } else {
                sensEvThSecLight++;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Not in use
    }
}
