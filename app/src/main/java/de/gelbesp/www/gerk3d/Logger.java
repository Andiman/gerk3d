package de.gelbesp.www.gerk3d;

import android.util.Log;
import android.widget.TextView;

import java.util.Vector;

public class Logger {

    private TextView textView;
    private int maxLines;

    private Vector<String> logs;

    public Logger(TextView textView, int maxLines) {
        this.textView = textView;
        this.maxLines = maxLines;

        logs = new Vector<>();
    }

    public void addLog(String msg) {

        logs.add(msg);

        while(logs.size() > maxLines) {
            logs.remove(0);
        }

        Log.i("LOGGER", msg);

        textView.setText(toText());
    }

    public String toText() {
        StringBuilder sb = new StringBuilder();
        for(String msg : logs) {
            sb.append(msg);
            sb.append('\n');
        }
        return new String(sb);
    }

}
